package collections.task1;

import shared.NotImplementedException;

/**
 * LIFO collection implementation.
 * @param <E> type of elements to store
 */
public class Stack<E> {

    /**
     * Adds new element to the end of stack.
     * @param element element to add
     */
    public void push(E element) {
        throw new NotImplementedException();
    }

    /**
     * Removes the last element added to stack and returns it.
     * @throws java.util.EmptyStackException in case stack is empty
     * @return the last element added to stack
     */
    public E pop() {
        throw new NotImplementedException();
    }

    /**
     * Returns the last element added to stack without removing it.
     * @throws java.util.EmptyStackException in case stack is empty
     * @return the last element added to stack
     */
    public E peek() {
        throw new NotImplementedException();
    }

    /**
     * Returns the number of elements in stack.
     * @return number of element in stack
     */
    public int size() {
        throw new NotImplementedException();
    }

    /**
     * Checks if stack is empty.
     * @return true if stack is empty, false - otherwise
     */
    public boolean isEmpty() {
        throw new NotImplementedException();
    }
}